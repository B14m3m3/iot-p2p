We have all used unix related operating systems to run the programs so we created a bash script to run the code.
Gradle should then be able to build it fully and run the program.
There is a 15 sec cooldown on each node being started.

To run the programs:
./xtermNodes.sh {number of nodes}

To close the programs you can use:
./killNodes.sh

- if no "number of nodes" are put, it defaults to 1 node.
- It will open nodes on port 8080 - (8080 + number of nodes)

Notes:
The successorList displays pretty randomly currently, though it is the correct 2 nodes that appear.
For testing replication use "localhost:8080/api/ping" to add a entry on the given node
