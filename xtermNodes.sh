#!/usr/bin/env bash
if [[ $@ -eq null ]]; then
xterm -e gradle bootRun -PjvmArgs="-Dserver.port=8080 -Dhost.value=127.0.0.1:8080" &
else
for ((i=8080; i<=8080+$@-1; i++))
do
xterm -e gradle bootRun -PjvmArgs="-Dserver.port=$i -Dhost.value=127.0.0.1:8080 -Xmx64m" &
sleep 15
done
fi
