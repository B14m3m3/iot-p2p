#!/usr/bin/env bash
JAVA_OPTS='-Djava.awt.headless=true'
JAVA_OPTS='-Xmx1024m'
if [[ $@ -eq null ]]; then
nohup gradle bootRun -PjvmArgs="-Xmx50m -Dserver.port=8080 -Dhost.value=127.0.0.1:8080"  &
echo 'Starting 127.0.0.1:8080'
else
for ((i=8080; i<=8080+$@-1; i++))
do
nohup gradle bootRun -PjvmArgs="-Xmx50m -Dserver.port=$i -Dhost.value=127.0.0.1:8080" &
echo 'Starting 127.0.0.1:'$i
sleep 5
done
fi
