package P2P.Setup;

import P2P.Interfaces.Setup;
import P2P.Podos.Peer;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SimpleSetup implements Setup {
    @Override
    public Peer createInitialPeer(String port) throws UnknownHostException {
        String ownIp = InetAddress.getByName("localhost").getHostAddress();
        String address = ownIp + ":" + port;
        int intport = Integer.parseInt(port);
        return new Peer(intport,address);
    }
}
