package P2P.Setup;

import P2P.Interfaces.Setup;
import P2P.Podos.Peer;
import org.apache.commons.codec.digest.DigestUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;


public class HashSetup implements Setup {
    @Override
    public Peer createInitialPeer(String port) throws UnknownHostException{
        String ownIp = InetAddress.getByName("localhost").getHostAddress();
        System.out.println(ownIp);
        String address = ownIp + ":" + port;
        String id = DigestUtils.sha1Hex(ownIp + ":" + port);
        int intid = Integer.parseInt(id.substring(0, 3),16);

        return new Peer(intid, address);
    }
}
