package P2P;

import P2P.Podos.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

@RestController
public class ApiController {

    @Autowired
    private Chord chord;


    @RequestMapping(method = {RequestMethod.GET}, path = "api/ping")
    public Ping ping() {
        chord.getDatabase().addData(new PhotonData(50,50,"hello",chord.getPeer().getId()));
        return new Ping();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/peer")
    public Peer peer() {
        return chord.getPeer();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/peer/successor")
    public Peer query(@RequestParam(value = "key") int key,
                      @RequestParam(value = "steps", defaultValue = "0") int steps) {
        Peer succ = chord.lookup(key, steps);
        return succ;
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/peer/successor/simple")
    public SimplePeer simpleLookup(@RequestParam(value = "key") int key ){
        Peer peer = chord.lookup(key,0);
        String[] part = peer.getAddress().split(":");
        SimplePeer ret = new SimplePeer(peer.getId(), part[0]);
        ret.setPort(Integer.parseInt(part[1]));

        return ret;
    }

    @RequestMapping(method = {RequestMethod.PUT}, path = "api/peer/successor")
    public QueryResponse updatePred(@RequestParam(value = "succ") String successor) {
        chord.setSuccessor(successor);
        return new QueryResponse();
    }

    @RequestMapping(method = {RequestMethod.PUT}, path = "api/peer/predecessor")
    public QueryResponse updateSucc(@RequestParam(value = "pred") String predecessor) {
        chord.setPredeccessor(predecessor);
        return new QueryResponse();
    }

    @RequestMapping(method = {RequestMethod.PUT}, path = "api/peer/replicate")
    public QueryResponse replicate(@RequestParam(value = "address") String address,
                                   @RequestParam(value = "id") int id){
        chord.replicate(address, id);
        return new QueryResponse();
    }



    @RequestMapping(method = {RequestMethod.POST}, path = "api/peer/successorList")
    public QueryResponse successorList(@RequestParam(value = "id") int id,
                                       @RequestParam(value = "address") String address,
                                       @RequestParam(value = "successorList") JSONObject successorList,
                                       @RequestParam(value = "ttl") int ttl) {
        SuccessorList successorList1 = chord.getSuccessorList();
        successorList1.updateSuccessorList(id, address, successorList, ttl);
        return new QueryResponse();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/peer/successorList")
    public HashMap<Integer, String> getSuccessorList() {
        return chord.getSuccessorList().getSuccessorList();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/peer/successorList/simple")
    public ArrayList<SimplePeer> simpleSuccessor(){
        ArrayList<SimplePeer> result = new ArrayList<>();

        LinkedHashMap<Integer,String> temp = chord.getSuccessorList().getSuccessorList();
        for(Integer i : temp.keySet()){
            String addr = temp.get(i);
            String[] part = addr.split(":");

            SimplePeer peer = new SimplePeer(i, part[0]);
            peer.setPort(Integer.parseInt(part[1]));
            result.add(peer);
        }

        return result;
    }



    @RequestMapping(method = {RequestMethod.POST}, path = "api/peer/fingertable")
    public Peer fingertable(@RequestParam(value = "id") int id,
                            @RequestParam(value = "fingerTable") JSONObject fingerTable) {

        FingerTable fingerTable1 = chord.getFingerTable();
        fingerTable1.updateFingerTable(id, fingerTable);
        return chord.getPeer();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/peer/fingertable")
    public TreeMap<Integer, String> getFingertable() {
        return chord.getFingerTable().getFingerTable();
    }

    @RequestMapping(method = {RequestMethod.POST}, path = "api/data")
    public QueryResponse setData(@RequestBody String data) {
        JSONObject jsonData = new JSONObject(data);
        Integer lightValue = (Integer)jsonData.get("lightValue");
        Integer photonID = (Integer)jsonData.get("photonID");
        String time = LocalDateTime.now().toString();
        PhotonData pd = new PhotonData(lightValue, photonID, time, chord.getPeer().getId());
        System.out.println(pd.toString());
        chord.getDatabase().addData(pd);

        return new QueryResponse();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/data")
    public ArrayList<PhotonData> getData() {
        return chord.getDatabase().getData();
    }

    @RequestMapping(method = {RequestMethod.GET}, path = "api/data/{id}")
    public ArrayList<PhotonData> getSpecificData(@PathVariable("id") int id){
        System.out.println(id);
        int[] arr = new int[1];
        arr[0] = id;
        return chord.getDatabase().getData(arr);
    }

    @RequestMapping(method = {RequestMethod.PUT}, path = "api/data")
    public QueryResponse putData(@RequestBody String data){
        JSONObject jsonData = new JSONObject(data);
        Integer state = jsonData.getInt("state");
        Integer photonID = jsonData.getInt("photonID");
        if(state != 0){
            chord.chordConnected(photonID, true);
        }else{
            chord.chordConnected(photonID, false);
        }

        return new QueryResponse();
    }




}
