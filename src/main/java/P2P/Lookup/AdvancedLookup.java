package P2P.Lookup;


import P2P.Chord;
import P2P.Interfaces.Lookup;
import P2P.Podos.Peer;
import P2P.SuccessorList;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

import java.util.*;

public class AdvancedLookup implements Lookup {
    private Chord chord;
    private LinkedHashMap<Integer, String> successorList;
    private TreeMap<Integer, String> fingerTable;
    Peer peer;


    public AdvancedLookup(Chord chord) {
        this.chord = chord;
    }

    @Override
    public Peer lookup(int queryId, int steps) {

        successorList = chord.getSuccessorList().getSuccessorList();
        fingerTable = chord.getFingerTable().getFingerTable();
        Peer peer = chord.getPeer();
        int myID = peer.getId();


        steps++;
        if(myID == queryId){
            peer.setSteps(steps);
            return peer;
        }

        int succId;
        try {
            HttpResponse<JsonNode> response = Unirest.get("http://" + chord.getPeer().getSuccessor() + "/api/peer").asJson();
            JsonNode body = response.getBody();
            JSONObject source = body.getObject();
            succId = (Integer) source.get("id");
            Peer successPeer = new Peer(succId, (String) source.get("address"));
            successPeer.setSuccessor((String) source.get("successor"));
            successPeer.setPredecessor((String) source.get("predecessor"));
            //check if I'm the best then QueryHit


            if(withinBounds(peer.getId(),queryId,successPeer.getId())){
                successPeer.setSteps(steps);
                return successPeer;
            }

        } catch (Exception e) {
            System.out.println("error in making contact to appraching server[AdvancedLookup]");
        }

        return checkFingerTable(queryId, steps);
    }

    private boolean withinBounds(int myID, int queryId, int succesorId){
        if (myID < queryId && queryId <= succesorId) {
            return true;
        } else if (myID < queryId && succesorId <= myID) {
            return true;
        }else if (succesorId == myID) {
            return true;
        }else if (queryId < succesorId && succesorId <= myID){
            return true;
        }else return queryId == myID;

    }

    private Peer defaultLookup(int queryId, int steps) {
        try {
            HttpResponse<JsonNode> requestResponse = Unirest.get("http://" + chord.getPeer().getSuccessor() + "/api/peer/successor")
                    .field("key", queryId)
                    .field("steps", steps)
                    .asJson();
            JSONObject peerResp = requestResponse.getBody().getObject();
            Peer p = new Peer((Integer) peerResp.get("id"), (String) peerResp.get("address"));
            p.setPredecessor((String) peerResp.get("predecessor"));
            p.setSuccessor((String) peerResp.get("successor"));
            p.setSteps(peerResp.getInt("steps"));
            return p;
        } catch (UnirestException e) {
            System.out.println("Advanced Lookup defaultLookup Successor call failure on node: " + queryId);
        }

        System.out.println("couldn't find any1 in lookup [defaultLookup]");
        return null;
    }


    public Peer checkFingerTable(int queryId, int steps) {
        fingerTable = chord.getFingerTable().getFingerTable();
        int myId = chord.getPeer().getId();
        try {
            for (int i = fingerTable.size() - 1; 0 <= i; i--) {
                String fingerAddress = (String) fingerTable.values().toArray()[i];
                int fingerId = (int) fingerTable.keySet().toArray()[i];
                if (myId < fingerId && fingerId <= queryId) {
                    return query(fingerAddress, queryId, steps);
                } else {
                    if (queryId < myId && queryId < fingerId) {
                        query(fingerAddress, queryId, steps);
                        return query(fingerAddress, queryId, steps);
                    }
                }
            }
        } catch (UnirestException e) {
            System.out.println("error in connecting to finger");
        }

        return defaultLookup(queryId, steps);
    }

    public void checkSuccessorList(int queryId, int steps) {
        List<Integer> keys = new ArrayList<>(successorList.keySet());
        if (keys == null || keys.size() < 2) {
            return;
        }
        for (int i = 1; i < keys.size(); i++) {
            String peerAddress = (String) successorList.values().toArray()[i];
            try {
                Peer nextSucc = nextSuccID(peerAddress);
                defaultLookup(queryId, steps);
            } catch (UnirestException e) {
                Object firstElement = successorList.keySet().iterator().next();
                successorList.remove(firstElement);
                System.out.println("error in checkSuccessorList");
            }
        }
    }

    public Peer nextSuccID(String peerAddress) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get("http://" + peerAddress + "/api/peer").asJson();
        JsonNode body = response.getBody();
        JSONObject source = body.getObject();
        Peer successPeer = new Peer((Integer) source.get("id"), (String) source.get("address"));
        successPeer.setSuccessor((String) source.get("successor"));
        successPeer.setPredecessor((String) source.get("predecessor"));
        return successPeer;
    }

    public Peer query(String peerAddress, int queryId, int steps) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get("http://" + peerAddress + "/api/peer/successor")
                .field("key", queryId)
                .field("steps", steps)
                .asJson();
        JsonNode body = response.getBody();
        JSONObject source = body.getObject();
        Peer peer = new Peer((Integer) source.get("id"), (String) source.get("address"));
        peer.setSuccessor((String) source.get("successor"));
        peer.setPredecessor((String) source.get("predecessor"));
        peer.setSteps((Integer) source.get("steps"));
        return peer;

    }
}
