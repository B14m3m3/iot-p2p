package P2P.Lookup;

import P2P.Chord;
import P2P.Interfaces.Lookup;
import P2P.Podos.Peer;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

import java.util.HashMap;


public class SimpleLookup implements Lookup {
    private Chord chord;

    public SimpleLookup(Chord chord){
        this.chord = chord;
    }

    @Override
    public Peer lookup(int queryId, int steps) {

        int myID = chord.getPeer().getId();
        try {
            HttpResponse<JsonNode> response = Unirest.get("http://" + chord.getPeer().getSuccessor() + "/api/peer").asJson();
            JsonNode body = response.getBody();
            JSONObject source = body.getObject();
            Peer successPeer = new Peer((Integer)source.get("id"),(String)source.get("address"));
            successPeer.setSuccessor((String) source.get("successor"));
            successPeer.setPredecessor((String) source.get("predecessor"));

            int succesorId = Integer.parseInt(source.get("id").toString());

            steps++;
            successPeer.setSteps(steps);
            HashMap<Peer, Integer> tuple = new HashMap<>();
            tuple.put(successPeer, steps);
            // is the queryId in between two existing nodes?

            if (myID < queryId && queryId <= succesorId) {
                return successPeer;
            } else {
                if (myID < queryId && succesorId <= myID) {

                    return successPeer;
                } else if (succesorId == myID) {
                    return successPeer;
                } else if (queryId < succesorId && succesorId <= myID) {
                    return successPeer;
                } else if(queryId == myID){
                    return successPeer;
                }else {
                    System.out.println("else [simpleLookup]");
                    HttpResponse<JsonNode> requestResponse = Unirest.get("http://" + chord.getPeer().getSuccessor() + "/api/peer/successor")
                            .field("key", queryId)
                            .field("steps", steps)
                            .asJson();

                    JSONObject peerResp = requestResponse.getBody().getObject();

                    Peer p = new Peer((Integer)peerResp.get("id"),(String)peerResp.get("address"));
                    p.setPredecessor((String) peerResp.get("predecessor"));
                    p.setSuccessor((String) peerResp.get("successor"));
                    p.setSteps(peerResp.getInt("steps"));
                    tuple = new HashMap<>();
                    tuple.put(successPeer, steps);
                    return p;
                }
            }
        } catch (UnirestException e) {
            System.out.println("something went wrong![lookup]");
        }
        return null;
    }
}