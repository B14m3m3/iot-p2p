package P2P;

import P2P.Podos.Peer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.Unirest;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.TreeMap;


public class FingerTable {

    private TreeMap<Integer, String> fingerTable = new TreeMap<>();
    private Chord chord;

    public FingerTable(Chord chord) {
        this.chord = chord;
    }



    public void initFingerTableUpdate(){
        Peer peer = chord.getPeer();
        fingerTableCall(peer.getId(), new TreeMap<>());
    }

    public void fingerTableCall(int id, TreeMap<Integer, String> finger) {
        try {
           // System.out.println("fingerTableCall");
            Unirest.post("http://" + chord.getPeer().getSuccessor() + "/api/peer/fingertable")
                    .field("id", id)
                    .field("fingerTable", String.valueOf(new JSONObject(finger))).asJson();
        } catch (Exception e) {
            System.out.println("Error forwarding the fingerTable");
        }
    }

    public void updateFingerTable(int id,  JSONObject fingerTable) {
        Gson gson = new Gson();
        Type type = new TypeToken<TreeMap<Integer, String>>() {}.getType();
        TreeMap<Integer, String> treeFingerTable = gson.fromJson(String.valueOf(fingerTable), type);

        int myID = chord.getPeer().getId();
        //Check if we have reached a full round
        if (myID == id) {
           // System.out.println("we have reached a full round");
            this.fingerTable = treeFingerTable;
        }else{
            for(int i = 1; i <= 11; i++) {
                int n = (int) Math.pow(2, i);
                int lowerBound = (id + n) % 4095;
                int upperBound = (id + n + n - 1) % 4095;

                if (withinFingerBounds(myID,lowerBound,upperBound)) {
                    treeFingerTable = findIdSpot(treeFingerTable, lowerBound, upperBound, myID);
                    break;
                }
            }
            fingerTableCall(id, treeFingerTable);
        }

    }

    public boolean withinFingerBounds(int value, int lowerBound, int upperBound){
        if(lowerBound <= value && value <= upperBound){
            return true;
        }
        //check edge of ring
        if(upperBound <= lowerBound) {
            if (lowerBound <= value) {
                return true;
            }
            if (value <= upperBound) {
                return true;
            }
        }
        return false;
    }

    public TreeMap<Integer, String> findIdSpot(TreeMap<Integer, String> treeFingerTable, int lowerBound, int upperBound, int myID) {
        Peer myPeer = chord.getPeer();

        if(lowerBound < upperBound){
            Integer currentTreeValue = treeFingerTable.higherKey(lowerBound);

            if(currentTreeValue == null){
                //none exist already so add myself
                treeFingerTable.put(myID,myPeer.getAddress());
            }else{

                if(currentTreeValue < upperBound){

                    //am i a better value?
                    if(myID < currentTreeValue){
                        treeFingerTable.remove(currentTreeValue);
                        treeFingerTable.put(myID,myPeer.getAddress());
                    }
                    //if the current value was better, keep that, so do nothing
                }else{
                    //no values in the range so add me
                    treeFingerTable.put(myID,myPeer.getAddress());

                }
            }
        }else{
            //----------------------|L|--c---x--|0|--------|U|-----------------
            //we must be at the edgecase between lower > 0 < upper & lower >  upper
            Integer underZeroBest = treeFingerTable.higherKey(lowerBound);
            Integer overZeroBest = treeFingerTable.higherKey(0);
            if(underZeroBest == null && overZeroBest == null){
                treeFingerTable.put(myID,myPeer.getAddress());
            }else if(underZeroBest == null){
                if(lowerBound < myID){
                    treeFingerTable.put(myID,myPeer.getAddress());
                    if(overZeroBest <= upperBound){
                        treeFingerTable.remove(overZeroBest);
                    }
                }else if(myID < upperBound){
                    if(overZeroBest <= upperBound && myID <= overZeroBest){
                        treeFingerTable.put(myID,myPeer.getAddress());
                        treeFingerTable.remove(overZeroBest);
                    }
                }
            }else{
                if(!(myID < upperBound)){
                    if(myID < underZeroBest){
                        treeFingerTable.remove(underZeroBest);
                        treeFingerTable.put(myID,myPeer.getAddress());
                    }
                }
            }
        }
        return treeFingerTable;
    }

    public TreeMap<Integer,String> getFingerTable() {
        return fingerTable;
    }
}
