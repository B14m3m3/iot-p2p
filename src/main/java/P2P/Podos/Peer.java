package P2P.Podos;


public class Peer {
    int ID;
    String address;
    String successor;
    String predecessor;
    int steps;

    public Peer(int ID, String address){
        this.ID = ID;
        this.address = address;
        this.successor = "None";
        this.predecessor = "None";
        this.steps = 0;

    }

    public int getId(){
        return ID;
    }

    public String getSuccessor(){
        return successor;
    }

    public String getPredecessor(){
        return predecessor;
    }

    public int getSteps(){
        return steps;
    }

    public String getAddress(){
        return address;
    }

    public void setSuccessor(String successor){
        this.successor = successor;
    }

    public void setPredecessor(String predecessor){
        this.predecessor = predecessor;
    }

    public void setSteps(int i){
        steps = i;
    }

    public String toString(){
        return  "ID: " + ID +
                ", Address: " + address +
                ", Successor: " + successor +
                ", Predecessor: " + predecessor;
    }

}
