package P2P.Podos;

public class LookupInfo {

    private Integer curLookupID = null;
    private String curLookupKey = null;
    private Integer hops = null;

    public LookupInfo(Integer curLookupID, String curLookupKey, Integer hops){
        this.curLookupID = curLookupID;
        this.curLookupKey = curLookupKey;
        this.hops = hops;
    }

    public Integer getCurLookupID() {
        return curLookupID;
    }

    public Integer getHops() {
        return hops;
    }

    public String getCurLookupKey() {
        return curLookupKey;
    }
}
