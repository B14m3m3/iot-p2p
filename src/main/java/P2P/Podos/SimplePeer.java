package P2P.Podos;


public class SimplePeer {
    public int id;
    public String address;
    public int port;

    public SimplePeer(int id, String address){
        this.id = id;
        this.address = address;
    }

    public int getid(){
        return id;
    }

    public String getAddress(){
        return address;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }
}
