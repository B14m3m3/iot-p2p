package P2P.Podos;


import java.sql.Time;
import java.time.LocalDateTime;

public class PhotonData {


    public int lightValue;
    public int photonID;
    public String time;
    public int loggerID;
    public PhotonData(int lightValue, int photonID, String time, int loggerID){
        this.lightValue = lightValue;
        this.photonID = photonID;
        this.loggerID = loggerID;
        this.time = time;
    }

    public int getlightValue() {
        return lightValue;
    }

    public void setLightValue(int lightValue){
        this.lightValue = lightValue;
    }

    public int getPhotonID() {
        return photonID;
    }

    public void setPhotonID(int photonID) {
        this.photonID = photonID;
    }

    public String getTime(){
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getLoggerID() {
        return loggerID;
    }

    public void setLoggerID(int loggerID) {
        this.loggerID = loggerID;
    }


    public String toString(){
        return "LightValue: " + lightValue +
                ", PhotonId: " + photonID +
                ", Time: " + time +
                ", LoggerId: " + loggerID;
    }

}
