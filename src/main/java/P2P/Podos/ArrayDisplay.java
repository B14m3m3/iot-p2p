package P2P.Podos;

/**
 * Created by b14m3m3 on 2/15/17.
 */
public class ArrayDisplay {
    public int lowerbound;
    public int upperbound;
    public int id;
    public String address;


    public ArrayDisplay(){

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAddress(String link) {
        this.address = link;
    }

    public void setLowerbound(int lowerbound) {
        this.lowerbound = lowerbound;
    }

    public void setUpperbound(int upperbound) {
        this.upperbound = upperbound;
    }

    public int getId() {
        return id;
    }

    public int getLowerbound() {
        return lowerbound;
    }

    public int getUpperbound() {
        return upperbound;
    }

    public String getAddress() {
        return address;
    }

}
