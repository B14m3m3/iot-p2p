package P2P;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.Unirest;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedHashMap;


public class SuccessorList {

    private LinkedHashMap<Integer,String> successorList = new LinkedHashMap<>();
    private int SUCCESSORLIST_LENGTH = 2;
    private Chord chord;


    public SuccessorList(Chord chord){
        this.chord = chord;
    }

    public void initSuccessorListUpdate() {
        successorListCall(chord.getPeer().getId(),chord.getPeer().getAddress(),new LinkedHashMap<>(),SUCCESSORLIST_LENGTH, false);
    }

    public void updateSuccessorList(int id, String address, JSONObject list, int ttl){
        Gson gson = new Gson();
        Type type = new TypeToken<LinkedHashMap<Integer,String>>() {}.getType();
        LinkedHashMap<Integer,String> successorList = gson.fromJson(String.valueOf(list), type);
        if(id == chord.getPeer().getId()){
          //  System.out.println(successorList.toString());
            this.successorList = successorList;
        }else{
          //  System.out.println("update");
            successorList.put(chord.getPeer().getId(), chord.getPeer().getAddress());
            if(ttl <= 1){
                successorListCall(id,address,successorList,ttl, true);
            }else{
                ttl--;
                successorListCall(id,address,successorList,ttl, false);
            }
        }
    }

    public void successorListCall(int id, String address, LinkedHashMap<Integer,String> list, int ttl, boolean end){
        String sendAddress = chord.getPeer().getSuccessor();
        if(end){
            //send to original peer instead
            sendAddress = address;
        }
        try {
          //  System.out.println("SuccessorListCall");
            Unirest.post("http://" + sendAddress + "/api/peer/successorList")
                    .field("id", id)
                    .field("address", address)
                    .field("successorList", String.valueOf(new JSONObject(list)))
                    .field("ttl", ttl + "")
                    .asJson();

        } catch (Exception e) {
            System.out.println("Error forwarding the SuccessorList");
        }
    }

    public LinkedHashMap<Integer,String> getSuccessorList() {
        return successorList;
    }
}
