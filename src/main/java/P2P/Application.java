package P2P;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * Run in terminal:
 * gradle bootRun -PjvmArgs="-Dserver.port=[port] -DTargetHost=[host] -DTargetPort=[port]"
 */

@SpringBootApplication
public class Application {
    static ConfigurableApplicationContext ctx;

    public static void main(String[] args) {
        ctx = SpringApplication.run(Application.class, args);
        Chord.getInstance().init();
    }

    public static void exit(){
        SpringApplication.exit(ctx);
    };

}

