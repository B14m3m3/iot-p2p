package P2P.DataAccess;

import P2P.Chord;
import P2P.Interfaces.DatabaseHandler;
import P2P.Podos.PhotonData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;

import java.lang.reflect.Type;
import java.sql.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

public class SQLite implements DatabaseHandler {
    private Chord chord;

    private String DB_ID = "photonID";
    private String DB_LIGHT = "lightvalue";
    private String DB_TIME = "timeslot";
    private String DB_LOGGER = "loggerID";
    public SQLite(Chord chord){
        this.chord = chord;
        try{
            Class.forName("org.sqlite.JDBC");
        }catch (Exception e){
            System.out.println("Fail setting up the JDBC [SQLite]");
        }
    }

    @Override
    public ArrayList<PhotonData> getData() {
        int[] arr = new int[0];
        return getData(arr);
    }


    @Override
    public ArrayList<PhotonData> getData(int[] args) {

        ArrayList<PhotonData> pd = new ArrayList<>();

        Connection connection = null;
        try{

            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + chord.getPeer().getId() + ".db");
            Statement statement = connection.createStatement();
            statement.executeUpdate("create table if not EXISTS photondata (" + DB_ID +" integer," + DB_LIGHT +" integer, " + DB_TIME +" String," + DB_LOGGER +" integer)");
            ResultSet rs;
            if(args.length > 0){
                rs = statement.executeQuery("select * from photondata WHERE " + DB_LOGGER + " = " + args[0]);
            }else{
                rs = statement.executeQuery("select * from photondata");
            }

            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            while(rs.next()){
                pd.add(new PhotonData(rs.getInt(DB_LIGHT),rs.getInt(DB_ID), rs.getString(DB_TIME), rs.getInt(DB_LOGGER)));
            }
        }catch (Exception e){
            System.out.println("error fetching stuff from db [getData] ");
            e.printStackTrace();
        } finally {
            try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
                // connection close failed.
                System.err.println(e);
            }
        }

        Collections.reverse(pd);
        return pd;
    }

    @Override
    public void addData(PhotonData pd) {
        // load the sqlite-JDBC driver using the current class loader
        Connection connection = null;
        try{

            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + chord.getPeer().getId() + ".db");

            Statement statement = connection.createStatement();
            statement.executeUpdate("create table if not EXISTS photondata (" + DB_ID +" integer," + DB_LIGHT +" integer, " + DB_TIME +" String," + DB_LOGGER +" integer)");

            PreparedStatement statement2 = connection.prepareStatement("INSERT INTO photondata ("+DB_ID+ ","+DB_LIGHT+","+ DB_TIME +"," +DB_LOGGER+") VALUES(?,?,?,?)");
            statement2.setInt(1,pd.getPhotonID());
            statement2.setInt(2, pd.getlightValue());
            statement2.setString(3, pd.getTime());
            statement2.setInt(4, pd.getLoggerID());
            statement2.executeUpdate();

            //statement.executeUpdate("insert into photondata values("+ pd.getPhotonID() +"," + pd.getlightValue() + "," + pd.getTime() +"," + pd.getLoggerID() + ")");
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
        }catch (Exception e){
            System.out.println("error adding stuff to db [addData] ");
            e.printStackTrace();
        } finally {
            try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
                // connection close failed.
                System.err.println(e);
            }
        }
    }

    @Override
    public void replicate(String address, int id) {
        Connection connection  = null;
        try{
            HttpResponse<JsonNode> response = Unirest.get("http://" + address + "/api/data/" + id).asJson();
            JsonNode body = response.getBody();
            JSONArray source = body.getArray();
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<PhotonData>>() {}.getType();
            ArrayList<PhotonData> pData = gson.fromJson(String.valueOf(source), type);

            connection = DriverManager.getConnection("jdbc:sqlite:" + chord.getPeer().getId() + ".db");
            Statement statement = connection.createStatement();
            statement.executeUpdate("create table if not EXISTS photondata (" + DB_ID +" integer," + DB_LIGHT +" integer, " + DB_TIME +" String," + DB_LOGGER +" integer)");
            statement.execute("DELETE FROM photondata WHERE " + DB_LOGGER + " = " + id);

            for(PhotonData pd : pData){
                PreparedStatement statement2 = connection.prepareStatement("INSERT INTO photondata ("+DB_ID+ ","+DB_LIGHT+","+ DB_TIME +"," +DB_LOGGER+") VALUES(?,?,?,?)");
                statement2.setInt(1,pd.getPhotonID());
                statement2.setInt(2, pd.getlightValue());
                statement2.setString(3, pd.getTime());
                statement2.setInt(4, pd.getLoggerID());
                statement2.executeUpdate();
            }
            System.out.println(pData.toString());
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("error fetching replication from address [replicate]");
        }

    }

    @Override
    public void requestReplicate() {
        LinkedHashMap<Integer,String> successorList = chord.getSuccessorList().getSuccessorList();
        try{
            for(String s : successorList.values() ){
                System.out.println(s);
                Unirest.put("http://" + s + "/api/peer/replicate")
                        .field("address", chord.getPeer().getAddress())
                        .field("id", chord.getPeer().getId() + "")
                        .asJson();
            }
        }catch(Exception e){
            System.out.println("error requesting replication");
            e.printStackTrace();
        }
    }

}
