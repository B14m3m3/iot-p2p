package P2P.DataAccess;

import P2P.Chord;
import P2P.Interfaces.DatabaseHandler;
import P2P.Podos.Peer;
import P2P.Podos.PhotonData;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.apache.tomcat.jni.Time;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.*;

public class MemoryDB implements DatabaseHandler {
    private HashMap<Integer, ArrayList<PhotonData>> list = new HashMap<>();
    private Chord chord;

    public MemoryDB(Chord chord) {
        this.chord = chord;

    }


    @Override
    public ArrayList<PhotonData> getData() {
        ArrayList<PhotonData> temp = new ArrayList<>();
        for(Integer k : list.keySet()){
            temp.addAll(list.get(k));
        }
        return temp;
    }

    @Override
    public ArrayList<PhotonData> getData(int[] args) {
        if(args.length > 0){
            return list.get(args[0]);
        }else{
            return list.get(chord.getPeer().getId());
        }
    }

    @Override
    public void addData(PhotonData pd) {
        String time = LocalDateTime.now().toString();
        ArrayList<PhotonData> photonList = list.get(chord.getPeer().
                getId()) != null ? list.get(chord.getPeer().getId()) : new ArrayList<>();
        photonList.add(pd);
        list.put(chord.getPeer().getId(), photonList);
    }

    @Override
    public void replicate(String address, int id) {
        try {
            System.out.println(address);
            HttpResponse<JsonNode> response = Unirest.get("http://" + address + "/api/data").asJson();
            JsonNode body = response.getBody();
            JSONArray source = body.getArray();
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<PhotonData>>() {
            }.getType();
            ArrayList<PhotonData> pData = gson.fromJson(String.valueOf(source), type);
            list.put(id, pData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error fetching replication from address [replicate]");
        }
    }

    @Override
    public void requestReplicate() {
        LinkedHashMap<Integer, String> successorList = chord.getSuccessorList().getSuccessorList();
        try {
            for (String s : successorList.values()) {
                System.out.println(s);
                Unirest.put("http://" + s + "/api/peer/replicate")
                        .field("address", chord.getPeer().getAddress())
                        .field("id", chord.getPeer().getId() + "")
                        .asJson();
            }
        } catch (Exception e) {
            System.out.println("error requesting replication");
            e.printStackTrace();
        }
    }
}
