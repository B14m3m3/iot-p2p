package P2P.Interfaces;

import P2P.Podos.Peer;

import java.net.UnknownHostException;

/**
 * Created by b14m3m3 on 2/5/17.
 */
public interface Setup {
    Peer createInitialPeer(String port) throws UnknownHostException;
}
