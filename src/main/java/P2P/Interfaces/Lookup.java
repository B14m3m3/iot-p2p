package P2P.Interfaces;


import P2P.Podos.Peer;

public interface Lookup {
    Peer lookup(int queryId, int steps);

}
