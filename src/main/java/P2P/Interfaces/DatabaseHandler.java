package P2P.Interfaces;

import P2P.Podos.PhotonData;

import java.util.ArrayList;

public interface DatabaseHandler {

    ArrayList<PhotonData> getData();

    ArrayList<PhotonData> getData(int[] args);

    void addData(PhotonData pd);

    void replicate(String address, int id);

    void requestReplicate();
}
