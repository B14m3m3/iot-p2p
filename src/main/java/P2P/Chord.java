package P2P;

import P2P.DataAccess.MemoryDB;
import P2P.DataAccess.SQLite;
import P2P.Interfaces.DatabaseHandler;
import P2P.Interfaces.Lookup;
import P2P.Interfaces.Setup;
import P2P.Lookup.AdvancedLookup;
import P2P.Podos.Peer;
import P2P.Podos.PhotonData;
import P2P.Setup.HashSetup;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;
import java.util.ArrayList;

@Component
public class Chord {


    private static Chord instance;

    private Setup setup;
    private Peer peer;
    private SuccessorList successorList;
    private FingerTable fingerTable;
    private UpdateWorker updateWorker;
    private Thread updateWorkerThread;
    private Lookup lookup;

    private DatabaseHandler database;

    private boolean photonState;


    @Value("${server.port}")
    private String ownPort;

    @Value("${host.value}")
    private String targetHost;


    @Autowired
    public Chord() {
        instance = this;
        setup = new HashSetup();
        successorList = new SuccessorList(this);
        fingerTable = new FingerTable(this);
        updateWorker = new UpdateWorker(this);
        lookup = new AdvancedLookup(this);
        //lookup = new SimpleLookup(this);
        updateWorkerThread = new Thread(updateWorker);
        database = new SQLite(this);
        //database = new MemoryDB(this);
    }

    public void init() {
        setup();
        join();
    }

    public void setup() {
        try {
            peer = setup.createInitialPeer(ownPort);
        } catch (UnknownHostException e) {
            System.out.println("This port is already in use please use another one");
            Application.exit();
        }
    }

    public void requestTableUpdates(){
        System.out.println("update called");
        successorList.initSuccessorListUpdate();
        fingerTable.initFingerTableUpdate();
        database.requestReplicate();
    }

    public void join() {
        try {
            if (targetHost.equals(peer.getAddress())) {
                // if you are trying to connect to yourself create a server
                peer.setPredecessor(getAddress());
                peer.setSuccessor(getAddress());
                updateWorkerThread.start();
                System.out.println("Created a new server with peer info: " + peer.toString());
            } else {
                HttpResponse<JsonNode> response = Unirest.get("http://" + targetHost + "/api/peer/successor").
                        field("key", peer.getId()).
                        field("steps", 0).
                        asJson();
                JsonNode body = response.getBody();
                JSONObject source = body.getObject();

                Peer successPeer = new Peer((Integer)source.get("id"),(String)source.get("address"));
                successPeer.setSuccessor((String) source.get("successor"));
                successPeer.setPredecessor((String) source.get("predecessor"));
                queryHit(successPeer);

            }
        } catch (Exception e) {
            System.out.println("Node could not be located");
        }
    }

    public void queryHit(Peer succ) throws UnirestException {
        // update new nodes own information         // 8081
        peer.setPredecessor(succ.getPredecessor());
        peer.setSuccessor(succ.getAddress());      // this is good

        // update successors predecessor
        Unirest.put("http://" + succ.getAddress() + "/api/peer/predecessor").
                field("pred", peer.getAddress()).
                asJson();
        // update predecessors successor
        Unirest.put("http://" + succ.getPredecessor() + "/api/peer/successor").
                field("succ", peer.getAddress()).
                asJson();
        updateWorkerThread.start();
    }
    public Peer lookup(int queryId, int steps) {
        return lookup.lookup(queryId, steps);
    }


    public void leave() {
        try {
            Unirest.put("http://" + peer.getSuccessor() + "/api/peer/predecessor").
                    field("pred", peer.getPredecessor()).
                    asJson();

            Unirest.put("http://" + peer.getPredecessor() + "/api/peer/successor").
                    field("succ", peer.getSuccessor()).
                    asJson();
            if(updateWorkerThread.isAlive()){
                updateWorker.setConnected(false);
                updateWorkerThread.interrupt();
            }
        } catch (Exception e) {
            System.out.println("Could not reach one of the peers [leave]");
        }
        Application.exit();
    }

    public SuccessorList getSuccessorList(){
        return successorList;
    }

    public FingerTable getFingerTable(){
        return fingerTable;
    }

    public void setPredeccessor(String predeccessor) {
        peer.setPredecessor(predeccessor);
    }

    public void setSuccessor(String successor) {
        peer.setSuccessor(successor);
    }

    public String getAddress() {
        return peer.getAddress();
    }

    public Peer getPeer() {
        return peer;
    }

    public static Chord getInstance() {
        return instance;
    }


    public void chordConnected(int photonID,boolean state) {
        photonState = state;
    }

    public boolean getPhotonState(){
        return photonState;
    }

    public void replicate(String address, int id){
        database.replicate(address, id);
    }

    public DatabaseHandler getDatabase(){
        return database;
    }
}
