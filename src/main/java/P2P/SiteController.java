package P2P;

import P2P.Podos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;


import java.util.*;

@Controller
public class SiteController {

    @Autowired
    private Chord chord;

    private LookupInfo lookupInfo = new LookupInfo(null,null,null);


    @ModelAttribute("integerObject")
    public IntegerObject getGreetingObject() {
        return new IntegerObject();
    }

    @GetMapping("/info")
    public String MainInfoPage(Model model) {
        return infoPage(model);
    }


    @RequestMapping("/leave")
    public String leave(){
        chord.leave();
        return "leave";
    }


    @PostMapping("/info")
    public String updatedInfoPage(Model model, @ModelAttribute IntegerObject integerObject) {
        Peer p = chord.lookup(integerObject.getKey(), -1);
        lookupInfo = new LookupInfo(p.getId(),p.getAddress(),p.getSteps());
        return infoPage(model);
    }

    public String infoPage(Model model){
        Peer peer = chord.getPeer();
        HashMap<Integer,String> succList = chord.getSuccessorList().getSuccessorList();
        TreeMap<Integer,String> fingerTable = chord.getFingerTable().getFingerTable();
        List<PhotonData> pdList = chord.getDatabase().getData();

        if(!(pdList.size() < 5)){
            pdList = pdList.subList(0,5);
        }

        ArrayList<ArrayDisplay> finger = generateFingerDisplay(fingerTable, peer);
        ArrayList<SimplePeer> suc = generateSuccessorDisplay(succList);

        model.addAttribute("successorList", suc);
        model.addAttribute("fingerList", finger);


        model.addAttribute("PeerId", peer.getId());
        model.addAttribute("Successor", peer.getSuccessor());
        model.addAttribute("Predeccessor", peer.getPredecessor());
        model.addAttribute("LocalAddress", peer.getAddress());


        model.addAttribute("curLookupID", lookupInfo.getCurLookupID());
        model.addAttribute("curLookupKey", lookupInfo.getCurLookupKey());
        model.addAttribute("hops", lookupInfo.getHops());

        model.addAttribute("photonData", pdList);
        model.addAttribute("photonConnected", chord.getPhotonState());

        return "info";
    }

    public ArrayList<SimplePeer> generateSuccessorDisplay(HashMap<Integer,String> successorList){
        ArrayList<SimplePeer> ad = new ArrayList<>();
        for(HashMap.Entry<Integer, String> entry : successorList.entrySet()) {
            ad.add(new SimplePeer(entry.getKey(),entry.getValue()));
        }

        return ad;
    }

    public ArrayList<ArrayDisplay> generateFingerDisplay(TreeMap<Integer,String> fingerTable, Peer peer){
        ArrayList<ArrayDisplay> display  = new ArrayList<>();

        for(int i = 1; i <= 11; i++) {
            int n = (int) Math.pow(2, i);
            int lowerBound = (peer.getId() + n) % 4095;
            int upperBound = (peer.getId() + n + n - 1) % 4095;
            ArrayDisplay ad = new ArrayDisplay();
            ad.setLowerbound(lowerBound);
            ad.setUpperbound(upperBound);

            Integer higherKey = fingerTable.higherKey(lowerBound);
            if(higherKey != null){
                ad.setId(higherKey);
                ad.setAddress(fingerTable.get(higherKey));
            }else{
                ad.setId(peer.getId());
                ad.setAddress(peer.getAddress());
            }

            display.add(ad);
        }
        return display;

    }
}