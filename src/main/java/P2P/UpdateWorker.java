package P2P;


import java.util.Random;

public class UpdateWorker implements Runnable {
    private Chord chord;
    private int SLEEPTIMEMILLIS = 10000;
    private int randomRange = 10000;
    private Random random;
    private boolean connected = true;

    public UpdateWorker(Chord chord){
        random = new Random();
        this.chord = chord;
    }
    @Override
    public void run() {
        while(connected){
            try{
                chord.requestTableUpdates();
                int r = random.nextInt(randomRange);
                Thread.sleep(SLEEPTIMEMILLIS + r);
                //sleeps between 10 and 20 seconds
            }catch (InterruptedException e){
                System.out.println("Something went wrong in updateWorker");
            }
        }
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
