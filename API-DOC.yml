# Example YAML to get you started quickly.
# Be aware that YAML has indentation based scoping.
# Code completion support is available so start typing for available options.
swagger: '2.0'

# This is your document metadata
info:
  version: "0.0.4"
  title: Chord API
  description: |
    Api written by team Sombra for the Chord system developed for  Internet of things / Peer-to-Peer

    Further restful properties have been inserted into the API and general cleanup along with added a few new apis

    Example query would be
      ```
      localhost:8080/api/peer
      ```
      which would return
      ```
      {
        ID: 123,
        address: "127.0.0.1:8080",
        successor: "127.0.0.1:8081",
        predecessor: "127.0.0.1:8079",
        steps: 10
      }
      ```

      Created by: Arnar Hardarson, Christian Nielsen, Steffen Jensen

host: localhost.com:8080
basePath: /api
schemes:
  - http

# Describe your paths here
paths:

#  /ping:
#    get:
#      description: |
#        Checks weather it can contact the other server.
#      responses:
#        200:
#          description: pong
#          schema:
#            properties:
#              response:
#                type: string
  /peer:
    get:
      description: |
        Gets the `Peer` description for the contacted peer.
      responses:
        200:
          description: Peer description
          schema:
              properties:
                ID:
                  type: integer
                address:
                  type: string
                successor:
                  type: string
                predecessor:
                  type: string
                steps:
                  type: integer
  /peer/successor:
    get:
      description: |
        Creates a query to find its successor
      parameters:
        - name: key
          in: query
          description: Your ID
          required: true
          type: integer
        - name: steps
          in: query
          description: steps
          required: false
          type: string

      responses:
        200:
          description: Peer description
          schema:
              properties:
                ID:
                  type: integer
                address:
                  type: string
                successor:
                  type: string
                predecessor:
                  type: string
                steps:
                  type: integer

    put:
      description: |
        Updates the given peers successor
      parameters:
        - name: succ
          in: query
          description: update peers given successor
          required: true
          type: string
      responses:
        200:
          description: OK
          schema:
              properties:
                response:
                  type: string


  /peer/predecessor:
    put:
        description: |
          Updates the given peers predecessor
        parameters:
          - name: pred
            in: query
            description: update peers given successor
            required: true
            type: string
        responses:
          200:
            description: OK
            schema:
                properties:
                  response:
                    type: string

  /peer/successor/simple:
    get:
      description: |
        A simplified lookup for the photon, with seperated port and address.
      parameters:
        - name: key
          in: query
          description: Your ID
          required: true
          type: integer
      responses:
        200:
          description: Peer data
          schema:
            properties:
                peer:
                  $ref: '#/definitions/photonPeer'

  /peer/successorList:
    post:
      description: |
        Creates a request to update our successorlist
      parameters:
        - name: id
          in: query
          description: id of the sender
          required: true
          type: integer
        - name: address
          in: query
          description: address of the sender
          required: true
          type: string
        - name: successorList
          in: body
          description: The currect successorlist of peer
          required: true
          schema:
            $ref: '#/definitions/simplePeer'
        - name: ttl
          in: query
          description: Time to live of request
          required: true
          type: integer

      responses:
        200:
          description: OK
          schema:
              properties:
                response:
                  type: string
    get:
      description: Gets the successorList of this node, can also use /fingerTable/{i} to get the i'th element in the table
      responses:
        200:
          description: Peer description
          schema:
              properties:
                ID:
                  type: array
                  items:
                    $ref: '#/definitions/simplePeer'


  /peer/successorList/simple:
    get:
      description: |
        A simplified lookup for the photon, with seperated port and address.
      parameters:
        - name: key
          in: query
          description: Your ID
          required: true
          type: integer
      responses:
        200:
          description: Peer data
          schema:
            properties:
                peer:
                  type: array
                  items:
                    $ref: '#/definitions/photonPeer'


  /peer/replicate:
    put:
      description: |
        Asks successors to replicate the peers data
      parameters:
        - name: address
          in: query
          description: The address of the peer
          required: true
          type: string
        - name: id
          in: query
          description: id of the peer
          required: true
          type: integer
      responses:
        200:
          description: OK
          schema:
              properties:
                response:
                  type: string



  /peer/fingerTable:
    post:
      description: |
        Creates a request to update our fingertable
      parameters:
        - name: id
          in: query
          description: id of the node sending the update
          required: true
          type: integer
        - name: fingertable
          in: body
          description: The currect successorlist of peer
          required: true
          schema:
            $ref: '#/definitions/simplePeer'

      responses:
        200:
          description: OK
          schema:
              properties:
                response:
                  type: string


    get:
      description: Gets the fingertable of this node, can also use /fingerTable/{i} to get the i'th element in the table
      responses:
        200:
          description: Peer description
          schema:
              properties:
                ID:
                  type: array
                  items:
                    $ref: '#/definitions/simplePeer'

  /data:
    get:
      description: |
        Gets the Photon data from the given peer
      responses:
        200:
          description: Peer data
          schema:
             properties:
                data:
                  type: array
                  items:
                    $ref: '#/definitions/data'

    post:
      description: |
        Sends data to the peer.
      parameters:
        - name: photon data
          in: body
          description: Sends current data to the connected peer
          required: true
          schema:
            $ref: '#/definitions/data'
      responses:
        200:
          description: OK
          schema:
              properties:
                response:
                  type: string
    put:
      description: |
        Updates the state of the photon on the connected peer
      parameters:
        - name: photonState
          in: body
          description: photonState
          required: true
          schema:
            $ref: '#/definitions/photonState'

      responses:
        200:
          description: OK
          schema:
              properties:
                response:
                  type: string
  /data/id:
    get:
      description: |
        Gets the Photon data from the given peer with given loggerid
      responses:
        200:
          description: Peer data
          schema:
             properties:
                data:
                  type: array
                  items:
                    $ref: '#/definitions/data'







definitions:
  simplePeer:
    type: object
    properties:
      id:
        type: integer
        description: id of the peer
      address:
        type: string
        description: address of the peer

  data:
    type: object
    properties:
      photonID:
        type: integer
        description: photonId of data
      lightValue:
        type: integer
        description: lightvalue of data

  photonState:
    type: object
    properties:
      photonID:
        type: integer
        description: photonId of data
      state:
        type: boolean
        description: state of photon

  photonPeer:
    type: object
    properties:
      ID:
        type: integer
        description: id of the peer
      address:
        type: string
        description: address of the peer
      port:
        type: integer
        description: port of the peer
