/*
 * Project Photon
 * Description:
 * Author:
 * Date:
 */
 #include "HttpClient.h"
 #include "application.h"
 #include <ArduinoJson.h>


 HttpClient http;
 // Headers currently need to be set at init, useful for API keys etc.
 http_header_t headers[] = {
     //  { "Content-Type", "application/json" },
     //  { "Accept" , "application/json" },
     { "Accept" , "*/*"},
     { NULL, NULL } // NOTE: Always terminate headers will NULL
 };
 http_request_t request;
 http_response_t response;

 int photoresistor = A0; // Read data from photoresistor on A0
 int power = A5; // Power til photoresistor on pin A5

 int analogvalue; // Variable for value of photoresistor
// setup() runs once, when the device is first turned on.

int photonID = 928;

String peerAdr = "192.168.43.118";
int peerPort = 8080;
String path = "api/data";
String lookupPath = "api/peer/successor/simple?key=928";
String successorPath = "api/peer/successorList/simple";

int connectedID = 5000;




int refreshDelay = 2000; //2 sec delay
int lookupWait = 5;
int lookupCounter = 5;


int succPort1;
String succAddr1;

int succPort2;
String succAddr2;

int strikes = 3;


void setup() {
  pinMode(photoresistor,INPUT);  // input to read photoresistor data
  pinMode(power,OUTPUT); // pin for power to the resistor
  digitalWrite(power,HIGH); // give it power

  Particle.variable("analogvalue", &analogvalue, INT); // sends the variable to the cloud

  Serial.begin(9600);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  if(lookupCounter >= lookupWait){
    lookupCounter = 0;
    lookup();
  }
  lookupCounter++;

  SendData();

  delay(refreshDelay);

}

void SendData(){
  analogvalue = analogRead(photoresistor);

  char sendingValue[64];
  sprintf(sendingValue, "{ \"lightValue\" : %d, \"photonID\": %d}", analogvalue, photonID);
  // Request path and body can be set at runtime or at setup.
  request.hostname = peerAdr;
  request.port = peerPort;
  request.path = path;
  // The library also supports sending a body with your request:
  //request.body = "{\"key\":\"value\"}";
  request.body = sendingValue;

  http.post(request, response, headers);
  Serial.println(peerAdr);
  Serial.println(peerPort);
  Serial.println(path);
  Serial.println(response.status);

}
void lookup(){

  StaticJsonBuffer<200> jsonBuffer;

  request.hostname = peerAdr;
  request.port = peerPort;
  request.path = lookupPath;

  // The library also supports sending a body with your request:
  //request.body = "{\"key\":\"value\"}";

  http.get(request, response, headers);

  Serial.println(response.status);
  if(response.status == 200){
    strikes = 3;
    String resp = response.body;
    Serial.println(resp);
    char response2[150];
    resp.toCharArray(response2, 150);

    JsonObject& root = jsonBuffer.parseObject(response2);

    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    int oldID = connectedID;
    int oldPort = peerPort;
    String oldAdr = peerAdr;


    int id = root["id"];
    const char* address = root["address"];
    String strAddress(address);
    //peerAdr = strAddress; TODO
    int port = root["port"];
    peerPort = port;


    if(id != connectedID){

      if(!(connectedID == 5000)){
        request.hostname = oldAdr;
        request.port = oldPort;
        request.path = path;

        char sendingValue[64];
        sprintf(sendingValue, "{ \"photonID\" : %d, \"state\": %d}", photonID, false);
        request.body = sendingValue;

        http.put(request, response, headers);
      }
      request.hostname = peerAdr;
      request.port = peerPort;
      request.path = path;

      char sendingValue[64];
      sprintf(sendingValue, "{ \"photonID\" : %d, \"state\": %d}", photonID, true);
      request.body = sendingValue;

      http.put(request, response, headers);


      connectedID = id;
    }
    getSuccessorList();
  }else{
    strikes--;
    if(strikes <= 0){
      if(succPort1 == peerPort){
        //peerAdr = succAddr2;
        peerPort = succPort2;
      }else{
        //peerAdr = succAddr1;
        peerPort = succPort1;
      }
      strikes = 3;
    }
  }

}


void getSuccessorList(){

  StaticJsonBuffer<200> jsonBuffer;

  request.hostname = peerAdr;
  request.port = peerPort;
  request.path = successorPath;


  http.get(request, response, headers);


  String resp = response.body;
  Serial.println(resp);
  char response2[150];
  resp.toCharArray(response2, 150);

  JsonArray& root = jsonBuffer.parseArray(response2);

  if (!root.success()) {
    Serial.println("parseObject() failed");
    return;
  }
  JsonObject& successor1 = root.at(0);
  JsonObject& successor2 = root.at(1);

  succPort1 = successor1["port"];
  const char* TempAddr1 = successor1["address"];
  succAddr1 = String(TempAddr1);

  succPort2 = successor2["port"];
  const char* TempAddr2 = successor2["address"];
  succAddr2 = String(TempAddr2);


}
